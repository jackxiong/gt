package objects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;

public class GumTreePage  {
	
	private WebDriver driver;

	public GumTreePage(WebDriver driver) {
		
			this.driver = driver;
	}
	
	public String gumTree_url() {
		
		String url  = "https://www.gumtree.com.au/s-ad/1266454340";
		return url;
	}
	
	public By gumtree_input_reply() {
		By input_replay = By.id("input-reply-widget-form-message");
		return input_replay;
	}
	
	public By send_message() {
		By sbutton = By.id("contact-seller-button");
		return sbutton;
	}
	
	public By contact_sell_button() {
		By sell = By.id("contact-seller-button");
		return sell;
	}
	
	
	public By close_message() {
		By cbutton = By.cssSelector(".icon-close");
		return cbutton;
	}
}
