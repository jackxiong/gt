package objects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Login {
	
	private WebDriver driver;
	
	public Login(WebDriver driver) {
		this.driver = driver;
	}
	
	public By user() {
		 By u = By.id("input-gumtree-sign-in-email-field");
		 return u;
	}
	public By password() {
		By p = By.id("input-gumtree-sign-in-password-field");
		
		return p;
	}
	public By submit() {
		By s = By.cssSelector(".gumtree-sign-in-form__submit-button");
		return s;
	}

}
