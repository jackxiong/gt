package base;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;

public class TestBaseSetup {
		
	  protected WebDriver browser;
	  static String driverPath = "./driver/";
	  
	  @BeforeClass(alwaysRun=true)
	  public void  baseSetup() {
		  
		    System.setProperty("webdriver.chrome.driver", driverPath+"chromedriver");
		    browser = new ChromeDriver();
		    browser.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	  }
	  
	  @AfterClass(alwaysRun=true)
	  public void afterClass() {
		  
		  browser.manage().deleteAllCookies();
		  browser.close();
		  browser.quit();
		  
	  }
}
