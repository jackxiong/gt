package test;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import base.TestBaseSetup;
import objects.GumTreePage;
import objects.Login;

public class TestGumtree extends TestBaseSetup {

	
	GumTreePage gumtree;
	Login login;
	@Test
	@Parameters({"message","user","pw"})
	public void test_verify_send_message_to_seller(String message,String user,String pw) {
		
		gumtree = new GumTreePage(browser);
		login   = new Login(browser);
		browser.get(gumtree.gumTree_url());
		
		//Assert elemment
		Assert.assertTrue(browser.findElement(gumtree.gumtree_input_reply()).isEnabled());
		
		WebElement ele = browser.findElement(gumtree.gumtree_input_reply());
		//send seller a message 
		JavascriptExecutor js = (JavascriptExecutor) browser;
		js.executeScript("arguments[0].value="+"'"+message+"'",ele);
		Assert.assertEquals(message, ele.getAttribute("value"));
		System.out.println(ele.getAttribute("value"));
		
		//click send message button 
		browser.findElement(gumtree.send_message()).click();
		browser.findElement(login.user()).sendKeys(user);
		browser.findElement(login.password()).sendKeys(pw);
		browser.findElement(gumtree.close_message()).click();
		
	}
}
